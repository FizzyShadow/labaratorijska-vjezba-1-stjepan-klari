﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class VremenskaZabilješka : Zabilješka
    {
        private DateTime trenutnoVrijeme;

        public VremenskaZabilješka(string autor, string tekst, string prioritet): base(autor, tekst, prioritet)
        {
            this.trenutnoVrijeme = DateTime.Now;
        }

        public VremenskaZabilješka() {
            trenutnoVrijeme = DateTime.Now;
        }

        public VremenskaZabilješka(string autor, string tekst, string prioritet, DateTime vrijeme) : base(autor, tekst, prioritet)
        {
            this.trenutnoVrijeme = vrijeme;
        }

        public DateTime Vrijeme
        {
            get { return this.trenutnoVrijeme; }
            set { this.trenutnoVrijeme = value; }
        }


        public override string ToString()
        {
            return this.getPrioritet() + ", " + this.getTekst() + ", " + this.getAutor() + "  " + this.trenutnoVrijeme.ToString();
        }
    }

    
}
